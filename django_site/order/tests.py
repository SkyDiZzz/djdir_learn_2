from order.views import order, order_new
from django.http import request, response
from django.test import TestCase
from django.db import models
from django.test.client import RequestFactory, Client
from .models import Customer, Message, Order
from menu.models import MenuItem
from .forms import CustomerForm

class TestModel(TestCase):

    @classmethod
    def setUpTestData(self):
        Customer.objects.create(law_firm_name='OOO Test', adress='Moscow, Lenina, 4', telephone=89876543210, full_name='Тест Тестович Тестов')
        MenuItem.objects.create(title='Бергер', info='Самый лучший бургер в мире', wprise=134.45, rprise=26.89)
        Message.objects.create(date='2020-12-11', purchase_amount=134.45)
        Order.objects.create(id_product=MenuItem.objects.get(id=1), id_customer=Customer.objects.get(id=1),
                            id_invoice=Message.objects.get(id=1), product_amount=5, product_prise=134.45)
        self.factory = RequestFactory()
    
    def test_label(self):
        customer = Customer.objects.get(id=1)
        menuitem = MenuItem.objects.get(id=1)
        message = Message.objects.get(id=1)
        odrer = Order.objects.get(id=1)

        field_label = customer._meta.get_field('law_firm_name').verbose_name
        self.assertEquals(field_label,'Наименование юр. лица')
        field_label = customer._meta.get_field('adress').verbose_name
        self.assertEquals(field_label,'Адресс')
        field_label = customer._meta.get_field('telephone').verbose_name
        self.assertEquals(field_label,'Телефон')
        field_label = customer._meta.get_field('full_name').verbose_name
        self.assertEquals(field_label,'ФИО контактного лица')

        field_label = menuitem._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'Наименование товара')
        field_label = menuitem._meta.get_field('info').verbose_name
        self.assertEquals(field_label,'Описание товара')
        field_label = menuitem._meta.get_field('wprise').verbose_name
        self.assertEquals(field_label,'Оптовая цена')
        field_label = menuitem._meta.get_field('rprise').verbose_name
        self.assertEquals(field_label,'Розничная цена')

        field_label = message._meta.get_field('date').verbose_name
        self.assertEquals(field_label,'Дата')
        field_label = message._meta.get_field('purchase_amount').verbose_name
        self.assertEquals(field_label,'Сумма')

        field_label = odrer._meta.get_field('id_product').verbose_name
        self.assertEquals(field_label,'id product')
        field_label = odrer._meta.get_field('id_customer').verbose_name
        self.assertEquals(field_label,'id customer')
        field_label = odrer._meta.get_field('id_invoice').verbose_name
        self.assertEquals(field_label,'id invoice')
        field_label = odrer._meta.get_field('product_amount').verbose_name
        self.assertEquals(field_label,'Количество')
        field_label = odrer._meta.get_field('product_prise').verbose_name
        self.assertEquals(field_label,'Цена')

    def test_form(self):
        request = self.factory.get('/order/new/', {'law_firm_name':'OOO Test two', 'adress':'Moscow, Lim, 2',
         'telephone':'89876543210', 'full_name':'Amm Ammme Ammmov'})
            
        response = order_new(request)
        return self.assertEquals(response.status_code, 200)

    def test_url(self):
        c = Client()
        request = c.get('/order/')
        return self.assertEquals(request.status_code, 200)
    