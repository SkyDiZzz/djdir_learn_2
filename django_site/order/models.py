from django.db import models
from menu.models import MenuItem

class Customer(models.Model):
    law_firm_name = models.CharField('Наименование юр. лица', max_length=250)
    adress = models.TextField('Адресс')
    telephone = models.IntegerField('Телефон')
    full_name = models.CharField('ФИО контактного лица', max_length=250)

    def __str__(self):
        return self.law_firm_name

    class Meta:
        verbose_name = 'Покупатель'
        verbose_name_plural = 'Покупатели'


class Message(models.Model):
    date = models.DateField('Дата')
    purchase_amount = models.FloatField('Сумма')

    def __str__(self):
        return self.date

    class Meta:
        verbose_name = 'Накладная'
        verbose_name_plural = 'Накладные'


class Order(models.Model):
    id_product = models.ForeignKey(MenuItem, on_delete=models.CASCADE)
    id_customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    id_invoice = models.ForeignKey(Message, on_delete=models.CASCADE)
    product_amount = models.IntegerField('Количество')
    product_prise = models.FloatField('Цена')



    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

# Create your models here.
