from datetime import date
from django import forms
from django.core.checks.messages import Error
from django.shortcuts import render, redirect
from django.http import request
from .forms import CustomerForm

def order(request):
    return render(request, 'order/order.html')

def order_new(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect('menu')
    else:
        form = CustomerForm()
    
    date = {
        'form': form,
    }
    return render(request, 'order/order_edit.html', date)
# Create your views here.
