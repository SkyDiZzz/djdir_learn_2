from django.contrib import admin

from .models import Customer, Order, Message

admin.site.register(Customer)
admin.site.register(Message)
admin.site.register(Order)
# Register your models here.
