from django import forms
from django.db import models
from django.db.models import fields
from .models import Customer

class CustomerForm(forms.ModelForm):
    
    class Meta:
        model = Customer
        fields = ('law_firm_name', 'adress', 'telephone', 'full_name')