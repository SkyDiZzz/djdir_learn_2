from django.shortcuts import render
from django.http import request
from .models import MenuItem

def menu_home(request):
    item = MenuItem.objects.all()
    return render(request, 'menu/menu_home.html', {'item': item})

def about(request):
    return render(request, 'menu/about.html')
# Create your views here.
