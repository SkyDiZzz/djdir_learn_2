from django.urls import path
from . import views


urlpatterns = [
    path('', views.menu_home, name='menu'),
    path('about', views.about, name='about'),
]