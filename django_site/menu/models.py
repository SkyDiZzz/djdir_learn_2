from django.db import models


class MenuItem(models.Model):
    title = models.CharField('Наименование товара', max_length=100)
    info = models.TextField('Описание товара')
    wprise = models.FloatField('Оптовая цена')
    rprise = models.FloatField('Розничная цена')


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
# Create your models here.
